Workbook DB
===========

## About
Takes workbooks (spreadsheets) and copies the data to a database.

Data structure is defined in `assets/data_struct.json`, note that the database
schema must be created first.

## Requirements

Python 3.6+ with modules:

* openpyxl
* cx_oracle
* sqlalchemy

Install these with `$ pip3 install -r requirements.txt`

## Usage

Use Workbook DB through the command line.

### Arguments

| Argument                             | Description                                           |
|--------------------------------------|-------------------------------------------------------|
| `-h`, `--help`                       | Display program help.                                 |
| `-n TNS`, `--tns TNS`                | Oracle TNS name, select which database to connect to. |
| `-u USERNAME`, `--username USERNAME` | Username for the database user                        |
| `-p`, `--with-password`              | Flag, if set, prompts for the user password.          |

Arguments can be provided through the following environmental variables:

| Environmental Variable | Description                                            |
|------------------------|--------------------------------------------------------|
| `WORKBOOKDB_TNS`       | Oracle TNS (Transparent Network Substrate) name for DB |
| `WORKBOOKDB_USERNAME`  | Username                                               |
| `WORKBOOKDB_PASSWORD`  | Password (Be aware of security implications!)          |

### Commands
#### Initialise Database `initdb`
Create the schema in the database.

`$ python workbookdb.py -n dbname -u myname initdb`

#### List Datasets `list`
List the workbooks that have been imported into the database.

`$ python workbookdb.py -n dbname -u myname list`

#### Import `import`
Import a workbook, either an Excel workbook (*.xlsx) or an Open Document workbook (*.ods).

Arguments:

* workbook: file path to the workbook to import

Example:

`$ python workbookdb.py -n dbname -u myname import ~/Documents/Workbook.xlsx`

#### Export `export`
Import an entry in the database, either to an Excel workbook (*.xlsx) or an Open Document workbook (*.ods).

Arguments:

* id: identifier of the 
* workbook: destination file path of where they're exported.

Example:

`$ python workbookdb.py -n dbname -u myname export 42 ~/Documents/Workbook.xlsx`
