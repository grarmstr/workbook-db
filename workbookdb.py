import os
import argparse
from getpass import getpass
from workbookdb import execute, COMMANDS


DESCRIPTION = """Takes a workbook and uploads it to an Oracle database.

Arguments can be provided through the following environmental variables:
    WORKBOOKDB_TNS          Oracle TNS (Transparent Network Substrate) name for DB
    WORKBOOKDB_USERNAME     Username
    WORKBOOKDB_PASSWORD     Password (Be aware of security implications!)
"""

EPILOG = "Copyright ⓒ 2020 CERN Industrial Controls and Safety Group"


def main():
    parser = argparse.ArgumentParser(description=DESCRIPTION, formatter_class=argparse.RawDescriptionHelpFormatter, epilog=EPILOG)
    parser.add_argument('-n', '--tns', help="Oracle TNS name of the DB server.")
    parser.add_argument('-u', '--username', help="Username for DB.")
    parser.add_argument('-p', '--with-password', action='store_true', help="If set, prompt for password.")
    parser.add_argument('command', nargs='+', help="Action to perform, commands are: %s" % ', '.join(COMMANDS))
    args = parser.parse_args()

    if args.command[0] not in COMMANDS:
        print("Fatal: Invalid command '%s'." % args.command[0])
        print("Valid commands are: %s" % ' '.join(COMMANDS))
        return 1

    tns = args.tns or os.getenv("WORKBOOKDB_TNS")
    if not tns:
        print("Fatal: No TNS  supplied")
        return 1

    username = args.username or os.getenv("WORKBOOKDB_USERNAME")
    if not username:
        print("Fatal: No username supplied")
        return 1

    if args.with_password:
        password = getpass("Password for '%s'@'%s':" % (username, tns))
    else:
        password = os.getenv("WORKBOOKDB_PASSWORD", "")
        if not password:
            print("Warning: no password given, using empty password.")

    execute(tns, username, password, args.command[0], args.command[1:])


if __name__ == '__main__':
    main()
