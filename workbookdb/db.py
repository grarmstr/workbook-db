from sqlalchemy import create_engine


URL_FORMAT: str = "{dialect}+{driver}://{username}:{password}@{tns}"
MAX_IDENTIFIER_LENGTH=128


def get_url(tns, username, password, dialect='oracle', driver='cx_oracle'):
    return URL_FORMAT.format(
        dialect=dialect,
        driver=driver,
        username=username,
        password=password,
        tns=tns,
    )


def get_engine(url, max_identifier_length=MAX_IDENTIFIER_LENGTH):
    return create_engine(url, max_identifier_length=max_identifier_length)
