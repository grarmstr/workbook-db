from .db import get_url, get_engine

COMMANDS = ['initdb', 'list', 'import', 'export']


def execute(tns, username, password, command, arguments):
    """
    Execute a command from WorkbookDB

    :param tns: Oracle TNS name of the DB server.
    :type tns: string
    :param username: Username for DB.
    :type username: string
    :param password: Password for DB.
    :type password: string
    :param command: Action to perform, valid commands are shown in the list COMMANDS.
    :type command: string
    :param arguments: List of arguments specific to a command
    :type arguments: list
    """
    url = get_url(tns, username, password)
    engine = get_engine(url)

    if command == 'initdb':
        raise NotImplementedError

    if command == 'list':
        from .cmd_list import run_list
        return run_list(engine)

    if command == 'import':
        raise NotImplementedError

    if command == 'export':
        raise NotImplementedError
